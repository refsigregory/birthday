<?php
/*
* Created By Refsi Sangkay
* Contact: facebook.com/refsi.sangkay
*/
require_once 'include/config.php';
?>
<!-- Created By Refsi Sangkay -->
<!DOCTYPE html>
<html lang="en"><head>
  <meta charset="utf-8">
  <title>BirthDay</title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  
  <meta property="og:title" content="">
	<meta property="og:type" content="website">
	<meta property="og:url" content="">
	<meta property="og:site_name" content="">
	<meta property="og:description" content="">

  <!-- Styles -->
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/animate.css">
  <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900|Montserrat:400,700' rel='stylesheet' type='text/css'>
  

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/main.css">

  <script src="js/modernizr-2.7.1.js"></script>
  
</head>

<body>
              <?php
                $query = $db->query("SELECT * FROM `daftar_nama` where month(tanggal)=month(NOW()) and day(tanggal) >= day(now()) and day(tanggal) <= (day(now())+6)");
                if($query->num_rows>0){
              ?>
    <header>
         <div class="col-sm-3">
             <div class="panel panel-default">
                          <div class="panel-heading">
                            <h3 class="panel-title">Daftar Ulang Tahun</h3>
                          </div>
                          <div class="panel-body">
                          <div class="show" style="padding: 14px 16px;border: solid 1px #e2e2e2;"></div>
              <?php
                while ($data = $query->fetch_assoc()){
                  $tanggal = date("d", strtotime($data['tanggal']));
                  $bulan = date("m", strtotime($data['tanggal']));
                  $tahun = date("Y", strtotime($data['tanggal']));

                  $umur = date("Y")-$tahun;

                  if($bulan == '01'){
                    $bulan = "Januari";
                  }else if($bulan == '02'){
                    $bulan = "Februari";
                  } else if($bulan == '03'){
                    $bulan = "Maret";
                  } else if($bulan == '04'){
                    $bulan = "April";
                  } else if($bulan == '05'){
                    $bulan = "Mei";
                  } else if($bulan == '06'){
                    $bulan = "Juni";
                  } else if($bulan == '07'){
                    $bulan = "Juli";
                  } else if($bulan == '08'){
                    $bulan = "Agustus";
                  } else if($bulan == '09'){
                    $bulan = "September";
                  } else if($bulan == '10'){
                    $bulan = "Oktober";
                  } else if($bulan == '11'){
                    $bulan = "November";
                  } else if($bulan == '12'){
                    $bulan = "Desember";
                  }

              ?>
                              <div class="show<?php echo $data['id'];?>" style="padding: 14px 16px;border: solid 1px #e2e2e2;" data-id="<?php echo $data['id'];?>"><img src="images/<?php echo $data['foto'];?>" width="60px"> <?php echo $data['nama'];?></div>
              <?php
              }
              ?>
                          </div>
                        </div>
          </div>
</header>   <?php
            }
            ?>
       
    <header>
      <div class="container">
        <div class="row">
        
        <div class="row header-info">
          <div class="col-sm-10 col-sm-offset-1 text-center">
            <div class="slide">
              <?php
                $query = $db->query("SELECT * FROM `daftar_nama` where month(tanggal)=month(NOW()) and day(tanggal) >= day(now()) and day(tanggal) <= (day(now())+6)");
                if($query->num_rows>0){
                while ($data = $query->fetch_assoc()){
                  $tanggal = date("d", strtotime($data['tanggal']));
                  $bulan = date("m", strtotime($data['tanggal']));
                  $tahun = date("Y", strtotime($data['tanggal']));

                  $umur = date("Y")-$tahun;

                  if($bulan == '01'){
                    $bulan = "Januari";
                  }else if($bulan == '02'){
                    $bulan = "Februari";
                  } else if($bulan == '03'){
                    $bulan = "Maret";
                  } else if($bulan == '04'){
                    $bulan = "April";
                  } else if($bulan == '05'){
                    $bulan = "Mei";
                  } else if($bulan == '06'){
                    $bulan = "Juni";
                  } else if($bulan == '07'){
                    $bulan = "Juli";
                  } else if($bulan == '08'){
                    $bulan = "Agustus";
                  } else if($bulan == '09'){
                    $bulan = "September";
                  } else if($bulan == '10'){
                    $bulan = "Oktober";
                  } else if($bulan == '11'){
                    $bulan = "November";
                  } else if($bulan == '12'){
                    $bulan = "Desember";
                  }

              ?>
                <div class="slider-item" data-id="<?php echo $data['id'];?>">
                <img src="images/<?php echo $data['foto'];?>" width="150px">
                  <h1 class="wow fadeIn"><?php echo $data['nama'];?></h1>

                  <p class="lead wow fadeIn" data-wow-delay="0.5s">Ulang Tahun yang ke-<?php echo $umur;?> pada <?php echo $tanggal;?>  <?php echo $bulan;?>  <?php echo date("Y");?> </p>
                </div>
              <?php
              }
            }else {
              ?>
                <p class="lead wow fadeIn" data-wow-delay="0.5s">Tidak ada yang berhari ulang tahun selama 7 hari kedepan ini. </p>
            <?php
            }
              ?>
            </div>
            
          </div>
        </div>
      </div>
    </header>
    
    <!-- Javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
    <script src="js/wow.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/refsi.js"></script>
    <script type="text/javascript">
      $('.slide').textSlider();

    </script>
   
    </body>
</html>
<!-- © Refsi Sangkay -->